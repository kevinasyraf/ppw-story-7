from django.test import TestCase
from django.urls import resolve
from .views import home

class Story7UnitTest(TestCase):
   def test_story7_url_is_exist(self):
      response = self.client.get('/')
      self.assertEqual(response.status_code, 200)

   def test_story7_using_index_func(self):
      found = resolve('/')
      self.assertEqual(found.func, home)

   def test_template_used(self):
      response = self.client.get('/')
      self.assertTemplateUsed(response, 'pages/index.html')
