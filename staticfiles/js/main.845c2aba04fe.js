$("#themeSwitch").on("click", () => {
  if ($("#themeSwitch").is(":checked")) {
    $(":root").css("--background-color", "rgba(8, 6, 6, 0.836)");
    $(":root").css("--dark-background-color", "rgba(250, 244, 244, 0.959)");
    $(":root").css("--text-color", "rgba(250, 244, 244, 0.959)");
  } else {
    $(":root").css("--background-color", "rgba(250, 244, 244, 0.959)");
    $(":root").css("--dark-background-color", "rgba(8, 6, 6, 0.836)");
    $(":root").css("--text-color", "rgba(8, 6, 6, 0.836)");
}
});

$(".accordion_tab").click(function(){
  if ($(this).parent().hasClass("active")){
    $(this).parent().removeClass("active");
    $(this).siblings(".accordion_content").slideUp(400);
  }
  else{ 
    $(this).parent().addClass("active");
    $(this).parent().siblings().removeClass("active");
    $(".accordion_tab").siblings(".accordion_content").slideUp(400);
    $(this).siblings(".accordion_content").slideDown(400);
  }
})
